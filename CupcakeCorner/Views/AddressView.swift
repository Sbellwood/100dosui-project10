//
//  AddressView.swift
//  CupcakeCorner
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct AddressView: View {
    @ObservedObject var wrapper: OrderWrapper
    
    var body: some View {
        Form {
            Section {
                TextField("Name", text: $wrapper.order.name)
                TextField("Steet Address", text: $wrapper.order.streetAddress)
                TextField("City", text: $wrapper.order.city)
                TextField("Zip", text: $wrapper.order.zip)
            }
            
            Section {
                NavigationLink(destination: CheckoutView(wrapper: wrapper)) {
                    Text("Check out")
                }
            }
            .disabled(wrapper.order.hasValidAddress == false)
        }
        .navigationBarTitle("Delivery Details", displayMode: .inline)
    }
}

struct AddressView_Previews: PreviewProvider {
    static var previews: some View {
        AddressView(wrapper: OrderWrapper())
    }
}
